﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowWindow : MonoBehaviour
{

	public Transform target;
	public Transform obj;
	public Vector3 zOffset = new Vector3 (0, 0, -10);
	public Rect window = new Rect (0, 0, 1, 1);

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 targetPos = target.position; //World coordinates

		Vector2 targetViewPos = Camera.main.WorldToViewportPoint (targetPos); //Convert to viewport coordinates

		Vector2 goalViewPos = window.Clamp (targetViewPos); //Clamp this position to the closest point inside the window

		Vector3 goalPos = Camera.main.ViewportToWorldPoint (goalViewPos); //Convert back to world coordinates

		//Convert both points into local coordinate system of the camera
		targetPos = transform.InverseTransformPoint (targetPos);
		goalPos = transform.InverseTransformPoint (goalPos);

		//Compute necessary camera movement in XY plane for target to appear at the goal
		Vector3 move = targetPos - goalPos;
		move.z = 0;

		transform.Translate (move);

		////////////////////////

		Vector3 objectivePosition = obj.position;

		Vector2 objectiveViewportPosition = Camera.main.WorldToViewportPoint (objectivePosition);

		Vector2 clampedObjectiveViewportPosition = window.Clamp(objectiveViewportPosition);

//		Debug.Log ("Objective Position in World Coordinates: " + objectivePosition);
//		Debug.Log ("Objective Position in Viewport Coordinates: " + objectiveViewportPosition);	
//		Debug.Log ("Objective Position in Clamped Viewport Coordinates: " + clampedObjectiveViewportPosition);
//		Debug.Log ("Orthographic size is : " + Camera.main.orthographicSize);

		if (objectiveViewportPosition.x < 0 || objectiveViewportPosition.y < 0 || objectiveViewportPosition.x > .9 || objectiveViewportPosition.y > .9) { //Check if objective is touching border of screen
			Camera.main.orthographicSize += .05f;
		} else if (Camera.main.orthographicSize >= 3.0f) //If camera size is greater than the starting value i.e. '3'
		{
			Camera.main.orthographicSize -= .05f; //Reduce the size of the camera until it reaches '3'.
		}
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.red;

		for (int i = 0; i < 4; i++) {
			Vector3 f = Camera.main.ViewportToWorldPoint (window.Corner (i)) - zOffset;
			Vector3 t = Camera.main.ViewportToWorldPoint (window.Corner (i + 1)) - zOffset;

			Gizmos.DrawLine (f, t);
		}
	}
}
