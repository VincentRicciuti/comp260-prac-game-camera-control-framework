﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{

	public float amplitude = 0.1f;
	public float frequency = 10f;
	public float duration = 0.5f;

	public float timer = 0f;
	private Vector3 direction = Vector3.zero;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (timer > 0) {
			timer -= Time.deltaTime;
			if (timer > 0) {
				float t = (duration - timer) * frequency;
				float a = amplitude * Mathf.Sin (t * Mathf.PI * 2);
				transform.localPosition = direction * a;
			} else {
				transform.localPosition = Vector3.zero;
			}
		}
	}

	public void Shake (Vector3 dir)
	{
		timer = duration;
		direction = dir.normalized;
	}
}
